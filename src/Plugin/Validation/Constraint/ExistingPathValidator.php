<?php

namespace Drupal\path_guard\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ExistingPath constraint.
 */
class ExistingPathValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  private $pathValidator;

  /**
   * Creates a new ExistingPathValidator instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PathValidatorInterface $path_validator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pathValidator = $path_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('path.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (empty($value->alias)) {
      return;
    }

    $url = $this->pathValidator->getUrlIfValid($value->alias);
    if (!$url) {
      return;
    }

    if ($url->isRouted()) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = !empty($value->getParent()) ? $value->getEntity() : NULL;

      if (!$entity) {
        return;
      }

      if ($entity->isNew()) {
        $this->context->addViolation($constraint->message);
      }
      else {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $original */
        $original = $this->entityTypeManager->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());

        if ($original->language()->getId() !== $entity->language()->getId()) {
          $original = $original->getTranslation($entity->language()->getId());
        }

        if ($original->path->alias !== $value->alias) {
          $this->context->addViolation($constraint->message);
        }
      }
    }
  }

}
