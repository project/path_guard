<?php

namespace Drupal\path_guard\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Existing path constraint.
 *
 * @Constraint(
 *   id = "ExistingPath",
 *   label = @Translation("Existing path", context = "Validation")
 * )
 */
class ExistingPath extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'The path is already in use.';

}
