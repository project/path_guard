<?php

namespace Drupal\Tests\path_guard\Functional;

use Drupal\Tests\path\Functional\PathTestBase;

/**
 * Tests path guard.
 *
 * @group path_guard
 */
class PathGuardTest extends PathTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['node', 'path', 'path_guard'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create test user and log in.
    $web_user = $this->drupalCreateUser([
      'create page content',
      'edit own page content',
      'create url aliases',
    ]);
    $this->drupalLogin($web_user);
  }

  /**
   * Tests the path form element.
   */
  public function testPathFormElement() {
    $assert_session = $this->assertSession();

    $this->drupalGet('node/add/page');

    // Tests existing path.
    $this->drupalPostForm('node/add/page', [
      'path[0][alias]' => '/node/add/page',
      'title[0][value]' => 'Page',
    ], 'Save');

    $assert_session->pageTextContains('The path is already in use.');

    // Tests not existing path.
    $this->drupalPostForm('node/add/page', [
      'path[0][alias]' => '/page',
      'title[0][value]' => 'Page',
    ], 'Save');

    $assert_session->pageTextContains('Basic page Page has been created.');

    // Tests updating node with the existing path.
    $this->drupalPostForm('node/1/edit', [
      'title[0][value]' => 'Page updated',
    ], 'Save');

    $assert_session->pageTextContains('Basic page Page updated has been updated.');

    // Tests omitting path.
    $this->drupalPostForm('node/add/page', [
      'title[0][value]' => 'Page 2',
    ], 'Save');

    $assert_session->pageTextContains('Basic page Page 2 has been created.');
  }

}
